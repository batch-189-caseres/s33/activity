
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => console.log(data));


fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	todo_titles = data.map((todo => {
		return todo.title
	}));
})
.then(() => console.log(todo_titles))


fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => {
	console.log(data);
	console.log(`The item "${data.title} on the list has a status of ${data.completed}"`)
});


fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		completed: false,
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "Updating the to do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data));


let today = new Date();
let date = `${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()}`

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: date,
		status: "Complete",
		title: "delectus aut autem",
		userId: 1  
	})
})
.then((response) => response.json())
.then((data) => console.log(data));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'

})
.then((response) => response.json())
.then((data) => console.log(data));
